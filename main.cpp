#include "EnvController.h"
#include <iostream>
#include <string>
#include <cstring>
#include <typeinfo>
#include <sstream>

using namespace std;

#include "DebugClient.h"

#define THREADS_NUM 20
#ifdef DEBUG
void * shared_debug_data;
#endif
void ::Map(const k1Base *const key,
                       const v1Base *const val) const {

    string* str = (string*)(((k1Imp *) key)->data);
//        DEBUG_PRINT("Running map on: ")
//        DEBUG_PRINT(*str)
    for(char& c : *str){
//            DEBUG_PRINT(c)
        k2Imp * new_char = new k2Imp();
        new_char->data = new string(1,c);
//            PRINT_OBJ(*new_char)
        v2Imp * new_one = new v2Imp();
        Emit2(new_char,new_one);
    }

//        DEBUG_PRINT(endl);
}
void MapReduceImp::Reduce(const k2Base *const key,
                          const V2_LIST &vals) const {

/* First Version.
        V2_LIST::const_iterator lst_it;
        int idx = 0;
        idx =0;
        string* s = (string*)(((k2Imp*)(key))->data);
//        DEBUG_PRINT(*s)
        for (lst_it = vals.begin();  lst_it != vals.end();  lst_it++, (idx)++){

        }
//        DEBUG_PRINT(", "<<idx<<". ")
        k3Imp* new_k = new k3Imp();
        new_k->data = ((k2Imp*)key)->data;
        v3Imp* new_v = new v3Imp();
        int Number = 123;       // number to be converted to a string

        string Result;          // string which will contain the result

        ostringstream convert;   // stream used for the conversion

        convert << idx;      // insert the textual representation of 'Number' in the characters in the stream

        // 'Result' now is equal to "123"
//    DEBUG_PRINT("YOOOOO"<<idx<<endl)
        new_v->data= new string( convert.str());
        Emit3(new_k,new_v);
*/

    ///*Do not delete (Second version)
    k3Imp* new_k = new k3Imp();
    new_k -> data = ((k2Imp*)key)->data;

    v3Imp* new_v = new v3Imp();
    new_v->data= new string(to_string(vals.size()));

    Emit3(new_k, new_v);

     //*/
}

using namespace std;

IN_ITEMS_LIST strings_to_k1v1_list;

IN_ITEMS_LIST convert_to_k1_list(list<string> in_list){

    k1Imp * k;
    v1Imp* v;
    //TODO warning escapes scope
    IN_ITEMS_LIST out_list;
    IN_ITEM pair_temp;

    for(auto s : in_list){
        k = new k1Imp();
        v = new v1Imp();
        k->data=new string(s);
        v->data=NULL;
        pair_temp = {k, v};
        out_list.push_back(pair_temp);
    }
    return out_list;
}

list<string> generate_random_list(int N){

    list<string> vecStr;

    for (int index=0; index<N; index++)
    {
        string str;
        for (int i = 0; i < 32; ++i)
        {
            int randomChar = rand()%(26+26+10);
            if (randomChar < 26)
                str += 'a' + randomChar;
            else if (randomChar < 26+26)
                str += 'A' + randomChar - 26;
            else
                str += '0' + randomChar - 26 - 26;
        }
        vecStr.push_back(str);
    }
    return vecStr;
}

int main9() {


//    list<string> str_lst = {"aaf", "aafsghash", "aafrtrte", "aafasdf", "aaffhaetus", "aafcvbsfrhre"};
//    list<string> str_lst = generate_random_list(20);
    //todo only accepts chunk*N size. fix!
    list<string> str_lst = {"PKdhtXMmr18n2L9K88eMlGn7CcctT9Rw",
                            "KSB1FebW397VI5uG1yhc3uavuaOb9vyJ",
                            "cXyHZzsRwpC5iUzahEcaYatja7kaqGHs",
                            "s6YhteYQrpJA76AcBAanBtuBoDzF97Vp",
                            "1HviLhW3uvrsqRsRft4GMp61QtuNod3f",
                            "Lomkv8dOus6I7opdFhJiuD7aXpObsHf3",
                            "TrccpfQHyKfvXswsA4ySxtTiIvi10nSJ",
                            "CUJPYonkWqDHH005UmNfGuocPw3FHKc9",
                            "uKOgYZqIeSgLI4EqghvKCHNf3ELyeLxz",
                            "laD7XUG1CKA8E4oKaHjAfWF6qetvQOSZ",
                            "OlVA3pptZQrtID1G9a6cUA6bEpui4c8G",
                            "lT6d8mwV0Mewd33c4XeMn9NSo50i5WPh",
                            "DJkzTEkJgm4igXk8InKWmmCAhqHddkiG",
                            "Rq6BUeaZr47xPftos38CdA1lQymRGumn",
                            "KgOskNhBFeWlkfxA7t3aTUjxgvfMNpXn",
                            "vCGFdL5IQRRYVeoQxfOeZYC3hFFVUt6f",
                            "TCIWeDuSjcG2eSIAYmCL82Fgx8ZfpVi6",
                            "mPSoib4pbyhdgNB2Y4CWW70i3PlhzudJ",
                            "9WXfVRvWfAXjcpc0jEK3zBbqenxBFBaC",
                            "lVH6A0QDpBNrQNfXfPPCeQSj1gIuFJWQ"};
//    list<string> str_lst = generate_random_list(10);
    IN_ITEMS_LIST starting_list = convert_to_k1_list(str_lst);
    DEBUG_PRINT("---printing list before map reduce---");
    DEBUG_PRINT(endl)
    for(auto pair:starting_list){
//        PRINT_OBJ(   *(k1Imp*)pair.first   )
        DEBUG_PRINT("::" << *(k1Imp *)pair.first << endl)
    }
//    IN_ITEM pair_temp;

    MapReduceImp* mprdc = new MapReduceImp();

    auto retval = runMapReduceFramework(*mprdc, starting_list, THREADS_NUM);
    DEBUG_PRINT("\n=== MapReduce Results ==="<<endl)
//    for(auto p : retval){
//        DEBUG_PRINT("pair: ( ")
//        PRINT_OBJ( *(k3Imp*)p.first  )
//        DEBUG_PRINT(" , ")
//        PRINT_OBJ( *(v3Imp*)p.second )
//        DEBUG_PRINT(" ) ")
//        DEBUG_PRINT(endl)
//    }



    DEBUG_PRINT("\nFinal K3V3 list: \n")
    for(auto pair : retval)
    {
        //auto shit = *((string*)((v3Imp*)pair.second));
        DEBUG_PRINT("(" << *(k3Imp*)pair.first)
        DEBUG_PRINT(", " << *((string*)((v3Imp*)pair.second)->data) << "), ")
    }

    DEBUG_PRINT("\n=== END ===")


    return 0;
}