#include "EnvController.h"

// #ifdef OS_2016_EX3_MACRO_DUMMY
#ifdef DEBUG
#include <iostream>
#include <map>
#include "MapReduceFramework.h"
#include "DebugClient.h"

using namespace std;
struct ltk2base_CMP
{
    bool operator()(const k2Base* s1, const k2Base* s2) const
    {
        return (*s1) < (*s2);
    }
};

//typedef std::pair<k2Base*, v2Base*> MID_ITEM;
typedef std::multimap<k2Base*, v2Base*, ltk2base_CMP> MID_ITEMS_MAP_CMP;
MID_ITEMS_MAP_CMP mid_tmp_map_CMP;

typedef std::list<v2Base*> SHUFFLE_VALUES_CMP;
typedef std::pair<k2Base*, SHUFFLE_VALUES_CMP> SHUFFLE_ITEM_CMP;
typedef std::list<SHUFFLE_ITEM_CMP> SHUFFLE_ITEMS_LIST_CMP;
SHUFFLE_ITEMS_LIST_CMP shuffle_list_CMP;

OUT_ITEMS_LIST out_items_list_CMP;


OUT_ITEMS_LIST runMapReduceFramework_CMP(MapReduceBase& mapReduce, IN_ITEMS_LIST& itemsList, int multiThreadLevel)
{

    DEBUG_PRINT("[DUMMY] runMapReduceFramework start [DUMMY]"<<endl)
    //map
    int index = 0;
    for ( IN_ITEMS_LIST::iterator it = itemsList.begin() ; it != itemsList.end(); it++ ,index++)
    {
//        DEBUG_PRINT(index)
         mapReduce.Map ((*it).first,(*it).second);
    }

    // no shuffle
//    DEBUG_PRINT("[DUMMY] shuffle [DUMMY]"<<endl)

    DEBUG_PRINT("CMP: "<<endl)
    for(auto wat:mid_tmp_map_CMP){
        DEBUG_PRINT(*(k2Imp*)wat.first)
    }

//    MID_ITEMS_MAP_CMP::iterator map_it, sub_it, temp_it;
//
//    // iter all keys in map:
//    index=0;
//    for (map_it = mid_tmp_map_CMP.begin();  map_it != mid_tmp_map_CMP.end();  map_it = temp_it, index++)
//    {
//        k2Base* theKey = (*map_it).first;
//        //get pair of iterators 'start' and 'end'
//        pair<MID_ITEMS_MAP_CMP::iterator, MID_ITEMS_MAP_CMP::iterator> keyRange = mid_tmp_map_CMP.equal_range(theKey);
//        //new list
//        std::list<v2Base*> val_list;
//
//        //create the list for reduce
//        for (temp_it =keyRange.first; temp_it != keyRange.second; ++temp_it){
//            val_list.push_back( (*temp_it).second );
//        }
//        //now reduce the list:
//        DEBUG_PRINT("reduce: ")
//        mapReduce.Reduce(theKey,val_list);
//        DEBUG_PRINT(endl);
//
//
//    }
//    OUT_ITEMS_LIST_CMP lst;
    return out_items_list_CMP;
}

void Emit2_CMP (k2Base* k , v2Base* v)
{
//    DEBUG_PRINT("insert: ")
    mid_tmp_map_CMP.insert({k, v});
//    DEBUG_PRINT("Number of elements with key ")
//    PRINT_OBJ(k)
//    DEBUG_PRINT(" count: "<< mid_tmp_map_CMP.count(k) << " end emit " <<endl)

}

void Emit3_CMP (k3Base* k, v3Base* v)
{
    out_items_list_CMP.push_back({k,v});
}

#endif