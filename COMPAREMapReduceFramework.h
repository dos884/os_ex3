#ifndef MAPREDUCEFRAMEWORK_HCMP
#define MAPREDUCEFRAMEWORK_HCMP

#include "EnvController.h"

//#include "MapReduceFramework.h"
#include <utility>

//typedef std::pair<k1Base*, v1Base*> IN_ITEM;
//typedef std::pair<k3Base*, v3Base*> OUT_ITEM;
//
//typedef std::list<IN_ITEM> IN_ITEMS_LIST;
//typedef std::list<OUT_ITEM> OUT_ITEMS_LIST;

OUT_ITEMS_LIST runMapReduceFramework_CMP(MapReduceBase& mapReduce, IN_ITEMS_LIST& itemsList, int multiThreadLevel);

void Emit2_CMP (k2Base*, v2Base*);
void Emit3_CMP (k3Base*, v3Base*);

#endif //MAPREDUCEFRAMEWORK_H
