#include "EnvController.h"

#ifdef OS_2016_EX3_MACRO_DUMMY
#include <iostream>
#include <map>
#include "DummyMapReduceFramework.h"
using namespace std;
struct ltk2base
{
    bool operator()(const k2Base* s1, const k2Base* s2) const
    {
        return (*s1) < (*s2);
    }
};

//typedef std::pair<k2Base*, v2Base*> MID_ITEM;
typedef std::multimap<k2Base*, v2Base*, ltk2base> MID_ITEMS_MULTIMAP;
MID_ITEMS_MULTIMAP shuffle_output_multimap;

typedef std::list<v2Base*> SHUFFLE_VALUES;
typedef std::pair<k2Base*, SHUFFLE_VALUES> SHUFFLE_ITEM;
typedef std::list<SHUFFLE_ITEM> SHUFFLE_ITEMS_LIST;
SHUFFLE_ITEMS_LIST shuffle_list;

OUT_ITEMS_LIST out_items_list;


OUT_ITEMS_LIST runMapReduceFramework(MapReduceBase& mapReduce, IN_ITEMS_LIST& itemsList, int multiThreadLevel)
{

    DEBUG_PRINT("[DUMMY] runMapReduceFramework start [DUMMY]"<<endl)
    //map
    int index = 0;
    for ( IN_ITEMS_LIST::iterator it = itemsList.begin() ; it != itemsList.end(); it++ ,index++)
    {
        DEBUG_PRINT("idx: "<<index)
         mapReduce.Map ((*it).first,(*it).second);
    }

    // no shuffle
    DEBUG_PRINT("[DUMMY] shuffle [DUMMY]"<<endl)

    MID_ITEMS_MULTIMAP::iterator map_it, sub_it, temp_it;

    // iter all keys in map:
    index=0;
    for (map_it = shuffle_output_multimap.begin();  map_it != shuffle_output_multimap.end();  map_it = temp_it, index++)
    {
        k2Base* theKey = (*map_it).first;
        //get pair of iterators 'start' and 'end'
        pair<MID_ITEMS_MULTIMAP::iterator, MID_ITEMS_MULTIMAP::iterator> keyRange = shuffle_output_multimap.equal_range(theKey);
        //new list
        std::list<v2Base*> val_list;

        //create the list for reduce
        for (temp_it =keyRange.first; temp_it != keyRange.second; ++temp_it){
            val_list.push_back( (*temp_it).second );
        }
        //now reduce the list:
        DEBUG_PRINT("reduce: ")
        mapReduce.Reduce(theKey,val_list);
//        DEBUG_PRINT(endl);


    }
//    OUT_ITEMS_LIST lst;
    return out_items_list;
}

void Emit2 (k2Base* k , v2Base* v)
{
//    DEBUG_PRINT("insert: ")
    shuffle_output_multimap.insert({k, v});
//    DEBUG_PRINT("Number of elements with key ")
//    PRINT_OBJ(k)
//    DEBUG_PRINT(" count: "<< shuffle_output_multimap.count(k) << " end emit " <<endl)

}

void Emit3 (k3Base* k, v3Base* v)
{
    out_items_list.push_back({k,v});
}

#endif